<?php
require_once 'bin/dbconnection.php';

/**
* Sprawdzanie czy użytkownik jest zalogowaniy
*
*/
if($user->isUserLoged()==true){
  $user->redirect('dashboard.php');
}

/**
* Obsługa formularza rejestracji
*
*/
if (isset($_POST['btn-register'])) {
  $username = trim($_POST['username']);
  $email = trim($_POST['email']);
  $password = trim($_POST['password']);

  $validateErrorMsg = $user->validateRegisterAction($username, $email, $password);

}

include 'templates/register.template.php';
?>
