<?php
session_start();
/**
* Kontroller obsługujący logowanie i rejestracje
*
*/
class UserController
{

  private $db;

  function __construct($dbh)
  {
    $this->db = $dbh;
  }
  /**
  * Obsługa logoawania
  *
  * @return void
  */
  public function loginAction($username, $email, $password){

    $stmt = $this->db->prepare("SELECT * FROM user WHERE username=:uname OR email=:uemail");
    $stmt->execute(array(':uname' => $username, ':uemail' => $email));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if (password_verify($password, $row['password'])) {
      $_SESSION['user'] = $row['id'];
      return true;
    } else {
      return false;
    }
  }

  /**
  * Pobiera dane do wyświetlenia w dashboardzie
  *
  * @return array
  */
  public function getUserDataAction(){
    $stmt = $this->db->prepare("SELECT * FROM user WHERE id=:id");
    $stmt->execute(array(':id' => $_SESSION['user']));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    return $row;
  }

  /**
  * Sprawdza czy użytkownik jest zalogowane
  *
  * @return bool
  */
  public function isUserLoged(){
    if (isset($_SESSION['user'])) {
      return true;
    } else {
      return false;
    }
  }

  /**
  * Wlidacja, wywołąnie metody register po pomyślnej walidacji
  *
  * @return string|void
  */
  public function validateRegisterAction($username, $email, $password){

    $flag = true;

    if(strlen($username)<6 || empty($username)) {
      $flag = false;
      $error = "Username must be at least 6 characters long";
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
      $flag = false;
      $error = "Your e-mail is incorect";
    }
    if (strlen($password)<8 || empty($password)){
      $flag = false;
      $error = "Password must be at lest 8 characters long";
    }

    $stmt = $this->db->prepare("SELECT username, email FROM user WHERE username=:uname OR email=:uemail");
    $stmt->execute(array(':uname' => $username, ':uemail' => $email));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if($row['username']==$username){
      $flag = false;
      $error = "That username already exists";
    } elseif ($row['email']==$email) {
      $flag = false;
      $error = "That email already exists";
    }

    if($flag===true){
      $this->registerAction($username, $email, $password);
    } else {
      return $error;
    }

  }

  /**
  * Dodanie usera do bazy danych
  *
  * @return void
  */
  public function registerAction($username, $email, $password){
    $password = password_hash($password, PASSWORD_DEFAULT);
    $stmt = $this->db->prepare("INSERT INTO `user`(`username`, `email`, `password`) VALUES (:uname, :uemail, :upass)");

    if($stmt->execute(array(':uname' => $username, ':uemail' => $email, ':upass' => $password ))){
      $this->redirect('index.php');
    }

  }
  /**
  * Przkierowanie do urla
  *
  * @return void
  */
  public function redirect($url){
    header("Location: $url");
  }

  /**
  * Wylogowanie usera
  *
  * @return void
  */
  public function logout(){
    session_unset();
  }

}

?>
