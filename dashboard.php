<?php
require_once 'bin/dbconnection.php';
/**
* Sprawdzanie czy użytkownik jest zalogowany
*/
if ($user->isUserLoged()===false) {
   $user->redirect('index.php');
}

/**
* Pobranie danych usera
*/
$data = $user->getUserDataAction();


include 'templates/dashboard.template.php';
?>
