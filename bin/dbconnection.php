<?php
$db_host = "localhost";
$db_user = 'root';
$db_pass = '';
$db_name = 'security';

try {
    $dbh = new PDO("mysql:host={$db_host};dbname={$db_name};charset=utf8", $db_user, $db_pass);

    $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Error!: " . $e->getMessage() . "<br/>";
    die();
}

include_once '/../UserController.php';
$user = new UserController($dbh)
?>
